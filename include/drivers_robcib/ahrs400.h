#ifndef AHRS400_H
#define AHRS400_H

#include <stdint.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>




/*** Generated messages ***/

#include <drivers_robcib/AHRS400_ANGLE.h>
#include <drivers_robcib/AHRS400_ANGLE_RAW.h>


/*** Definitions ***/

/*** AHRS constants ***/
#define AHRS_GYRO_RANGE (200 * M_PI / 180)
#define AHRS_G_RANGE 4
#define AHRS_DEFAULT_BAUDRATE B38400

#define AHRS_DATA_HEADER 0xFF
#define AHRS_MAX_MSG_SIZE 30
#define AHRS_ANGLE_PAYLOAD_LEN 28


/*** AHRS Message codes ***/
// Communication test messages
#define PING 'R'
#define PING_RESPONSE 'H'

// Measurement mode configuration messages
#define VOLTAGE_MODE 'r'
#define VOLTAGE_MODE_RESPONSE 'R'
#define SCALED_MODE 'c'
#define SCALED_MODE_RESPONSE 'C'
#define ANGLE_MODE 'a'
#define ANGLE_MODE_RESPONSE 'A'

// Communication mode configuration messages
#define POLLED_MODE 'P'
#define CONTINUOUS_MODE 'C'
#define REQUEST_DATA 'G'
#define REQUEST_BAUD 'b'
#define REQUEST_BAUD_RESPONSE 'B'
#define NEW_BAUD 'a'
#define NEW_BAUD_RESPONSE 'A'

// Information query messages
#define QUERY_VERSION 'v'
#define QUERY_VERSION_LENGTH 26
#define QUERY_SERIAL_NUMBER 'S'

// Magnetic calibration messages
#define START_CALIB 's'
#define START_CALIB_RESPONSE 'S'
#define END_CALIB 'u'
#define END_CALIB_RESPONSE 'U'
#define CLEAR_HARDI 'h'
#define CLEAR_HARDI_RESPONSE 'H'
#define CLEAR_SOFTI 't'
#define CLEAR_SOFTI_RESPONSE 'T'


class AHRS400
{
private:

    int port_id_;
public:
    AHRS400();
    ~AHRS400();

    typedef enum {
        AHRS_VOLTAGE_MODE,
        AHRS_SCALED_MODE,
        AHRS_ANGLE_MODE
    } ahrs_mode_t;


    /**
     * Get current time in microseconds since epoch.
     */
    static inline uint64_t get_time_us();

    /**
     * Open the AHRS serial port stream.
     * @param The path of the serial port device.
     * @return The AHRS port stream or NULL if error.
     */
    FILE* ahrs_open(const char *path);


    /**
     * Close the AHRS serial port stream.
     * @param AHRS400 serial port stream.
     */
    void ahrs_close();

    /**
     * Ping the AHRS.
     * @param AHRS400 serial port stream.
     * @return 0 if pong received, -1 if pong not received, if error or if EOF.
     */
    int ahrs_ping(FILE *file);

    /**
     * Put the AHRS in the continuous mode.
     * @param AHRS400 serial port stream.
     * @return 0 if success, -1 if error.
     */
    int ahrs_set_continuous(FILE *file);

    /**
     * Put the AHRS in the polled mode.
     * @param AHRS400 serial port stream.
     * @return 0 if success, -1 if error.
     */
    int ahrs_set_polled(FILE *file);

    /**
     * Request data to the AHRS in the polled mode.
     * @param AHRS400 serial port stream.
     * @return 0 if success, -1 if error.
     */
    int ahrs_request_data(FILE *file);

    /**
     * Flush the AHRS input/output stream.
     * @param AHRS400 serial port stream.
     * @return 0 if success, -1 if error.
     */
    int ahrs_purge(FILE *file);

    /**
     * Set the AHRS measurement mode.
     * @param AHRS400 serial port stream.
     * @param desired mode.
     * @return 0 if success received, -1 if error or EOF.
     */
    int ahrs_set_mode(FILE *file, ahrs_mode_t mode);

    /**
     * Get a message from the AHRS.
     * @param AHRS400 serial port stream.
     * @param packet payload size (without header or checksum).
     * @param[out] pointer to where the payload should be stored.
     * @param[out] reception time in microseconds since epoch.
     * @return 0 if message read and payload stored, -1 if error or EOF.
     */
    int get_msg(FILE *file, unsigned size, uint8_t *payload, uint64_t *recv_timestamp);

    /**
     * Get an angle mode message from the AHRS.
     * @param AHRS400 serial port stream.
     * @param Angle raw message payload.
     * @return 0 if message read and payload stored, -1 if error or EOF.
     */
    int ahrs_get_angle_raw(FILE *file, drivers_robcib::AHRS400_ANGLE_RAW *angle_raw);
    /**
     * Conver angles using scale.
     * @param raw   raw message payload.
     * @param scaled converted values
     */
    void ahrs_angle_conv(drivers_robcib::AHRS400_ANGLE_RAW *raw, drivers_robcib::AHRS400_ANGLE *scaled);

    /**
     * Search for an AHRS header in the stream.
     * @param AHRS400 serial port stream.
     * @return 0 if header found, -1 if error or EOF.
     */
    int search_header(FILE *file);

    /**
     * Calculate message checksum.
     * @param message payload.
     * @param message payload length (without header or checksum).
     * @return message checksum.
     */
    uint8_t checksum(uint8_t *payload, unsigned size);

    static inline float raw_to_angle(int16_t raw){
        return raw * M_PI / 32768.0;
    }


    static inline float raw_to_gyro(int16_t raw){
        return raw * 1.5 * AHRS_GYRO_RANGE / 32768.0;
    }


    static inline float raw_to_accel(int16_t raw){
        return raw * 1.5 * AHRS_G_RANGE * 9.8 / 32768.0;
    }


    static inline float raw_to_mag(int16_t raw){
        return raw * 1.5 * 1.25e-4 / 32768.0;
    }


    static inline float raw_to_temperature(uint16_t raw){
        return ((raw * 5 / 4096.0) - 1.375) * 44.44;
    }


    static inline float raw_to_time(int16_t raw){
        return -raw * 0.00000079;
    }
    static inline int16_t pack_int16(uint8_t *payload, unsigned index) {
        unsigned msb = index*2;
        unsigned lsb = index*2 + 1;
        return payload[lsb] + ((int16_t)payload[msb]<<8);
    }


    static inline uint16_t pack_uint16(uint8_t *payload, unsigned index) {
        return (uint16_t) pack_int16(payload, index);
    }

    int port_id() const;
    void setPort_id(int port_id);

};




#endif // AHRS400_H
