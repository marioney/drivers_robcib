/*
 * CMPDriver.h
 *
 *  Created on: Nov 19, 2012
 *      Author: Mario Garzon
 */

#ifndef DRIVER_CMPS03_HPP
#define DRIVER_CMPS03_HPP


/*** Standar C Libraries ***/
#include <termios.h>
#include <fcntl.h>

/*** ROS Libraries ***/
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <tf/tf.h>

#define P_ERROR -1

class CMPDriver {
public:
  CMPDriver();
  virtual ~CMPDriver();
/*** Functions ***/
  void spin(const ros::TimerEvent& e);

  int openPort(void);
  void closePort(int fd);
  int writeData(int fd, int nbytes);
  int readData(int fd, int nbytes);

  // Ros Variables
  sensor_msgs::Imu cmps_reading_; // Message to be published
  ros::Publisher cmps_data_pub_;	// Ros Publisher for the message
  ros::Timer timer_;				// Timer for spin function

  double freq;					// Controls the frequency for publishing the messages

  // Imu message covariances
  double linear_acceleration_stdev_;
  double orientation_stdev_;
  double angular_velocity_stdev_;

  std::string frame_id_;	// Frame id for the message
  std::string msg_topic;  // To store imu_topic
  std::string port_name_;  // To store port device name

  unsigned char sbufout[10];						// Stores data to be read
  unsigned char sbufin[10];						// Stores data to be written

  struct termios options;

  int port_id;




};

#endif // DRIVER_CMPS03_HPP
