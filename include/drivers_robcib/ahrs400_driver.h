#ifndef AHRS400_DRIVER_H
#define AHRS400_DRIVER_H


#include <drivers_robcib/ahrs400.h>

#include <argp.h>
#include <netdb.h>
#include <stdlib.h>
#include <sys/socket.h>



/*** ROS Libraries ***/
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <tf/tf.h>


/** Mavlink system identifier */
#define MAVLINK_SYSID 1

/** Mavlink compenent identifier, equal to MAV_COMP_ID_IMU */
#define MAVLINK_COMPID 200


class AHRS400Driver : AHRS400
{
public:
    AHRS400Driver();
    ~AHRS400Driver();

    /*** Functions ***/
    void spin(const ros::TimerEvent& e);
    void ahrs_to_msg(const drivers_robcib::AHRS400_ANGLE angle);


    FILE *ahrs_stream;
    // Ros Variables
    sensor_msgs::Imu cmps_reading_; // Message to be published
    ros::Publisher cmps_data_pub_;	// Ros Publisher for the message
    ros::Timer timer_;				// Timer for spin function

    double freq;					// Controls the frequency for publishing the messages

    // Imu message covariances
    double linear_acceleration_stdev_;
    double orientation_stdev_;
    double angular_velocity_stdev_;

    std::string frame_id_;	// Frame id for the message



};

#endif // AHRS400_DRIVER_H
