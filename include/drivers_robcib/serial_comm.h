/**
 * Device module for Crossbow's AHRS400 Attitude and Heading Reference System.
 */

#ifndef SERIAL_COMM_H
#define SERIAL_COMM_H

/*** Standar C Libraries ***/

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>


#include <drivers_robcib/ahrs400.h>



/*** AHRS constants ***/
#define AHRS_GYRO_RANGE (200 * M_PI / 180)
#define AHRS_G_RANGE 4
#define AHRS_DEFAULT_BAUDRATE B38400

#define AHRS_DATA_HEADER 0xFF
#define AHRS_MAX_MSG_SIZE 30
#define AHRS_ANGLE_PAYLOAD_LEN 28


/*** AHRS Message codes ***/
// Communication test messages
#define PING 'R'
#define PING_RESPONSE 'H'

// Measurement mode configuration messages
#define VOLTAGE_MODE 'r'
#define VOLTAGE_MODE_RESPONSE 'R'
#define SCALED_MODE 'c'
#define SCALED_MODE_RESPONSE 'C'
#define ANGLE_MODE 'a'
#define ANGLE_MODE_RESPONSE 'A'

// Communication mode configuration messages
#define POLLED_MODE 'P'
#define CONTINUOUS_MODE 'C'
#define REQUEST_DATA 'G'
#define REQUEST_BAUD 'b'
#define REQUEST_BAUD_RESPONSE 'B'
#define NEW_BAUD 'a'
#define NEW_BAUD_RESPONSE 'A'

// Information query messages
#define QUERY_VERSION 'v'
#define QUERY_VERSION_LENGTH 26
#define QUERY_SERIAL_NUMBER 'S'

// Magnetic calibration messages
#define START_CALIB 's'
#define START_CALIB_RESPONSE 'S'
#define END_CALIB 'u'
#define END_CALIB_RESPONSE 'U'
#define CLEAR_HARDI 'h'
#define CLEAR_HARDI_RESPONSE 'H'
#define CLEAR_SOFTI 't'
#define CLEAR_SOFTI_RESPONSE 'T'

#include <ros/ros.h>
#define P_ERROR -1


//#include <iostream>
class SerialComm
{
private:

    std::string port_name_;

    int baud_rate_;
    int parity_;
public:
    SerialComm(std::string port_name, int baud_rate, int parity);
    ~SerialComm();
    int openPort();
    void closePort(int fd);
    int writeData(int fd, int nbytes);
    int readData(int fd, int nbytes);

    unsigned char sbufout[10];						// Stores data to be read
    unsigned char sbufin[30];						// Stores data to be written


};

#endif // SERIAL_COMM_H
