#include <drivers_robcib/ahrs400.h>


int AHRS400::port_id() const
{
    return port_id_;
}

void AHRS400::setPort_id(int port_id)
{
    port_id_ = port_id;
}
AHRS400::AHRS400()
{

}

AHRS400::~AHRS400()
{
    ahrs_close();
}


uint64_t AHRS400::get_time_us()
{
    struct timespec t;
    if (clock_gettime(CLOCK_REALTIME, &t))
    {
        syslog(LOG_ERR,"Error getting time: %s", strerror(errno));
        return 0;
    }
    return (uint64_t)t.tv_sec * 1000000 + (uint64_t)t.tv_nsec / 1000;
}


FILE* AHRS400::ahrs_open(const char *path)
{
    int fd = open(path, O_RDWR);
    if (fd < 0) {
        const char *msg = "Error opening AHRS port `%s`: %s";
        syslog(LOG_ERR, msg, path, strerror(errno));
        return NULL;
    }

    FILE *file = fdopen(fd, "r+b");
    if (!file) {
        syslog(LOG_ERR, "Error in fdopen: %s", strerror(errno));
        close(fd);
        return NULL;
    }

    struct termios ahrs_termios;
    if (tcgetattr(fd, &ahrs_termios)
        || cfsetispeed(&ahrs_termios, AHRS_DEFAULT_BAUDRATE)
        || cfsetospeed(&ahrs_termios, AHRS_DEFAULT_BAUDRATE)
        || tcsetattr(fd, TCSANOW, &ahrs_termios)) {
        const char *msg = "Error setting AHRS serial port baud rate: %s";
        syslog(LOG_WARNING, msg, strerror(errno));
    } else {
        cfmakeraw(&ahrs_termios);
        if (tcsetattr(fd, TCSANOW, &ahrs_termios)) {
            const char *msg = "Error making AHRS serial port raw: %s";
            syslog(LOG_WARNING, msg, strerror(errno));
        }
    }

    setPort_id(fd);
    return file;
}

void AHRS400::ahrs_close()
{
     close(port_id());
}

int AHRS400::ahrs_ping(FILE *file)
{
    if (fputc(PING, file) == EOF) {
        syslog(LOG_ERR, "Error writing ping to AHRS stream: %s",
               strerror(errno));
        return -1;
    }

    printf("about to send ping");

    int response = fgetc(file);
    printf("got response");
    if (response == EOF) {
        if (feof(file))
            syslog(LOG_WARNING, "EOF while waiting for ping response");
        else
            syslog(LOG_ERR, "Read error while waiting for ping response: %s",
                   strerror(errno));
        return -1;
    }

    if (response != PING_RESPONSE) {
        syslog(LOG_INFO, "Invalid ping from AHRS: %#x", response);
        return -1;
    }

    return 0;
}



int AHRS400::ahrs_set_continuous(FILE *file)
{
    if (fputc(CONTINUOUS_MODE, file) == EOF) {
        syslog(LOG_ERR, "Error writing continuous mode to AHRS stream: %s",
               strerror(errno));
        return -1;
    }
    return 0;
}


int AHRS400::ahrs_set_polled(FILE *file) {
    if (fputc(POLLED_MODE, file) == EOF) {
        syslog(LOG_ERR, "Error writing polled mode to AHRS stream: %s",
               strerror(errno));
        return -1;
    }
    return 0;
}

int AHRS400::ahrs_request_data(FILE *file) {
    if (fputc(REQUEST_DATA, file) == EOF) {
        syslog(LOG_ERR, "Error writing request data to AHRS stream: %s",
               strerror(errno));
        return -1;
    }
    return 0;
}


int AHRS400::ahrs_purge(FILE *file) {
    int fd = fileno(file);
    if (fd < 0) {
        syslog(LOG_ERR, "Error getting file descriptor: %s", strerror(errno));
        return -1;
    }

    if (tcflush(fd, TCIOFLUSH)) {
        syslog(LOG_WARNING, "Error flushing stream: %s", strerror(errno));
        return -1;
    }

    return 0;
}




int AHRS400::ahrs_set_mode(FILE *file, ahrs_mode_t mode) {
    char mode_command, mode_response;

    switch (mode) {
    case AHRS_VOLTAGE_MODE:
        mode_command = VOLTAGE_MODE;
        mode_response = VOLTAGE_MODE_RESPONSE;
        break;
    case AHRS_SCALED_MODE:
        mode_command = SCALED_MODE;
        mode_response = SCALED_MODE_RESPONSE;
        break;
    case AHRS_ANGLE_MODE:
        mode_command = ANGLE_MODE;
        mode_response = ANGLE_MODE_RESPONSE;
        break;
    default:
        syslog(LOG_ERR, "Unknown AHRS mode");
        return -1;
    }

    if (fputc(mode_command, file) == EOF) {
        syslog(LOG_ERR, "Error writing mode command to AHRS stream: %s",
               strerror(errno));
        return -1;
    }

    int response = fgetc(file);
    if (response == EOF) {
        if (feof(file))
            syslog(LOG_WARNING, "EOF while waiting for mode response");
        else
            syslog(LOG_ERR, "Read error while waiting for mode response: %s",
                   strerror(errno));
        return -1;
    }

    if (response != mode_response) {
        syslog(LOG_INFO, "Invalid mode response from AHRS: %#x", response);
        return -1;
    }

    return 0;
}



int AHRS400::search_header(FILE *file) {
    for (;;) {
        //Get the next character from the stream
        int recv = fgetc(file);

        if (recv == AHRS_DATA_HEADER)
            return 0;

        if (recv == EOF) {
            if (feof(file))
                syslog(LOG_WARNING, "EOF while waiting for header");
            else
                syslog(LOG_ERR, "Read error while waiting for header: %s",
                       strerror(errno));
            return -1;
        }
    }
}


uint8_t AHRS400::checksum(uint8_t *payload, unsigned size) {
    uint8_t checksum = 0;
    for (int i=0; i<size; i++)
        checksum += payload[i];
    return checksum;
}



int AHRS400::get_msg(FILE *file, unsigned size, uint8_t *payload, uint64_t *recv_timestamp)
{
    uint8_t work[size + 1];
    uint8_t work_ptr = 0;
    bool header_found = false;

    for (;;) {
        // Look for header
        if (!header_found) {
            if (search_header(file))
                return -1;
        }

        // Save the time the header was found
        if (recv_timestamp)
            *recv_timestamp = get_time_us();

        // Get message body and checksum
        if (!fread(work + work_ptr, sizeof(work) - work_ptr, 1, file)) {
            if (feof(file))
                syslog(LOG_WARNING, "EOF while waiting for payload");
            else
                syslog(LOG_ERR, "Read error while waiting for payload: %s",
                       strerror(errno));
            return -1;
        }

        // Check checksum
        uint8_t recv_checksum = work[size];
        if (checksum(work, size) == recv_checksum) {
            // Valid message received, save output and return
            memcpy(payload, work, size);
            return 0;
        }

        // Invalid message, look for header in work buffer
        work_ptr = 0;
        header_found = false;
        for (int i=0; i<sizeof work; i++) {
            if (work[i] == AHRS_DATA_HEADER) {
                memmove(work, work + i + 1, sizeof(work) - i - 1);
                work_ptr = sizeof(work) - i - 1;
                header_found = true;
                break;
            }
        }
    }
}

int AHRS400::ahrs_get_angle_raw(FILE *file, drivers_robcib::AHRS400_ANGLE_RAW *angle_raw)
{
    uint8_t payload[AHRS_ANGLE_PAYLOAD_LEN];
    if (get_msg(file, sizeof payload, payload, &angle_raw->time_usec))
        return -1;

    angle_raw->roll = pack_int16(payload, 0);
    angle_raw->pitch = pack_int16(payload, 1);
    angle_raw->yaw = pack_int16(payload, 2);
    angle_raw->xgyro = pack_int16(payload, 3);
    angle_raw->ygyro = pack_int16(payload, 4);
    angle_raw->zgyro = pack_int16(payload, 5);
    angle_raw->xacc = pack_int16(payload, 6);
    angle_raw->yacc = pack_int16(payload, 7);
    angle_raw->zacc = pack_int16(payload, 8);
    angle_raw->xmag = pack_int16(payload, 9);
    angle_raw->ymag = pack_int16(payload, 10);
    angle_raw->zmag = pack_int16(payload, 11);
    angle_raw->temperature = pack_uint16(payload, 12);
    angle_raw->sensor_time = pack_uint16(payload, 13);
    return 0;
}

void AHRS400::ahrs_angle_conv(drivers_robcib::AHRS400_ANGLE_RAW *raw, drivers_robcib::AHRS400_ANGLE *scaled)
{

    scaled->time_usec = raw->time_usec;
    scaled->xacc = raw_to_accel(raw->xacc);
    scaled->yacc = raw_to_accel(raw->yacc);
    scaled->zacc = raw_to_accel(raw->zacc);
    scaled->xgyro = raw_to_gyro(raw->xgyro);
    scaled->ygyro = raw_to_gyro(raw->ygyro);
    scaled->zgyro = raw_to_gyro(raw->zgyro);
    scaled->xmag = raw_to_mag(raw->xmag);
    scaled->ymag = raw_to_mag(raw->ymag);
    scaled->zmag = raw_to_mag(raw->zmag);
    scaled->roll = raw_to_angle(raw->roll);
    scaled->pitch = raw_to_angle(raw->pitch);
    scaled->yaw = raw_to_angle(raw->yaw);
    scaled->temperature = raw_to_temperature(raw->temperature);
    scaled->sensor_time = raw->sensor_time;
}
