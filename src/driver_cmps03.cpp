/*
 * CMPDriver.cpp
 *
 *  Created on: Nov 19, 2012
 *      Author: Mario Garzon
 */

#include <drivers_robcib/driver_cmps03.hpp>


CMPDriver::CMPDriver() {

  // Create the node handler
  ros::NodeHandle nh_private("~");

  // Read the parameters or default values
  nh_private.param("freq",freq,15.0);
  nh_private.param("msg_topic",msg_topic,std::string("imu_data"));
  nh_private.param("port_name",port_name_,std::string("/dev/ttyIMU0"));

  nh_private.param("linear_acceleration_stdev", linear_acceleration_stdev_, 0.098);
  nh_private.param("orientation_stdev", orientation_stdev_, 0.035);
  nh_private.param("angular_velocity_stdev", angular_velocity_stdev_, 0.012);
  nh_private.param("frame_id", frame_id_, std::string("imu_link"));

  // Todo Define real values for each measure.

  double angular_velocity_covariance = angular_velocity_stdev_ * angular_velocity_stdev_;
  double orientation_covariance = orientation_stdev_ * orientation_stdev_;
  double linear_acceleration_covariance = linear_acceleration_stdev_ * linear_acceleration_stdev_;

  cmps_reading_.linear_acceleration_covariance[0] = linear_acceleration_covariance;
  cmps_reading_.linear_acceleration_covariance[4] = linear_acceleration_covariance;
  cmps_reading_.linear_acceleration_covariance[8] = linear_acceleration_covariance;

  cmps_reading_.angular_velocity_covariance[0] = angular_velocity_covariance;
  cmps_reading_.angular_velocity_covariance[4] = angular_velocity_covariance;
  cmps_reading_.angular_velocity_covariance[8] = angular_velocity_covariance;

  cmps_reading_.orientation_covariance[0] = orientation_covariance;
  cmps_reading_.orientation_covariance[4] = orientation_covariance;
  cmps_reading_.orientation_covariance[8] = orientation_covariance;

  cmps_reading_.header.frame_id = frame_id_;

  // Set Values to be sent for reading the data
  sbufout[0] = 0x55;						// USBI2C command to R/W single byte address devices
  sbufout[1] = 0xC1;						// CMPS03 address with R/W bit set high
  sbufout[2] = 0x00;						// Register we wish to start reading from
  sbufout[3] = 0x04;						// Read for 4 bytes

  cmps_data_pub_ = nh_private.advertise<sensor_msgs::Imu>(msg_topic,1);



  // Open Port
  port_id = openPort();
  if (port_id == P_ERROR)
  {
    ROS_ERROR("CMPS Driver: ROS Node is Shutting Down");
    ros::shutdown();
  }
//	closePort(port_id);

  timer_ = nh_private.createTimer(ros::Duration(1.0/freq), &CMPDriver::spin, this);

  ROS_INFO("CMPS Driver: ROS Setup Completed");
}

CMPDriver::~CMPDriver() {
  // TODO Auto-generated destructor stub+
  closePort(port_id);
}

void CMPDriver::spin(const ros::TimerEvent& e)
{
  ROS_DEBUG("CMPS Driver: In the loop at time :%f", ros::Time::now().toSec());
  //elapsed_time = e.current_real.toSec()-e.last_real.toSec();

  int bearing;
  double compass_reading;
/*	port_id = openPort();
  if (port_id == P_ERROR)
  {
    ROS_ERROR("CMPS Driver: ROS Node is Shutting Down");
    ros::shutdown();
  }*/


  if (writeData(port_id, 4) == 4) // Write this data to the USBI2C
  {
  //	ROS_INFO("CMPS Driver: Written");
    if (readData(port_id, 4) == 4)					// Read back the 4 bytes requested
    {
    //	ROS_INFO("CMPS Driver: Readed");
      bearing = ((sbufin[2] << 8) + sbufin[3])/10;		// Calculate bearing from high and low bytes

      bearing = 359 - bearing;

      compass_reading = double(bearing * M_PI)/180;		// Convert the reading to Radians

    //	ROS_INFO("CMPS Driver: Bearing - %d - %f",bearing ,compass_reading);

      cmps_reading_.header.stamp = ros::Time::now();  // Add timestamp

      tf::quaternionTFToMsg(tf::createQuaternionFromYaw(compass_reading), cmps_reading_.orientation); // Create quaternion from yaw

      cmps_data_pub_.publish(cmps_reading_);  // Publish the data

    }
  }
/*
  closePort(port_id);*/
}

int CMPDriver::openPort(void)
{
  int fd;										// File descriptor for the port

  fd = open(port_name_.c_str(), O_RDWR | O_NOCTTY );				// Open port for read and write not making it a controlling terminal
  if (fd == P_ERROR)
  {
    ROS_ERROR("CMPS Driver: Unable to open port %s, -- %s", port_name_.c_str(), strerror(errno));		// If open() returns an error
  }
  else
  {
    fcntl(fd, F_SETFL, 0);						// Get the current options for the port
    tcgetattr(fd, &options);

    cfsetispeed(&options, B19200);					// Set the baud rates to 19200

    options.c_cflag |= (CLOCAL | CREAD);				// Enable the receiver and set local mode
    options.c_cflag &= ~PARENB;					// No parity bit

    options.c_cflag &= ~CSTOPB;					// Set 2 stop bits

    options.c_cflag &= ~CSIZE;					// Set the character size
    options.c_cflag |= CS8;

    tcsetattr(fd, TCSANOW, &options);				// Set the new options for the port
  }
  return (fd);
}


void  CMPDriver::closePort(int fd)
{
  if(close(fd) == P_ERROR)						// Close the port if it returns an error then display an error report
  {
    ROS_ERROR("CMPS Driver: Unable to close port %s,-- %s", port_name_.c_str(), strerror(errno));
    //perror("closePort: Unable to close port ");
  }
}

int CMPDriver::writeData(int fd, int nbytes)
{
  int bytes;

  bytes = write(fd, sbufout, nbytes);					// Write nbytes of data from wbuf
  if(bytes == P_ERROR)							// If write returns an error (-1)
  {
    ROS_ERROR("writeData: Error while trying to write data - %s", strerror(errno));
  }
  else if(bytes != nbytes)
  {
    ROS_WARN("Only %u bytes written out of %u requested\n", bytes, nbytes);
  }
  return bytes;

}

int CMPDriver::readData(int fd, int nbytes)
{
  int bytes;

  bytes = 0;

  //ROS_INFO("CMPS Driver: Trying");
  bytes = read(fd, sbufin, nbytes);
  if(bytes == P_ERROR) // If read returns and error (-1)
  {
    ROS_ERROR("readData: Error while trying to read data - %s", strerror(errno));
  }
  else if(bytes != nbytes)
  {
    ROS_WARN("Only %u bytes read out of %u requested", bytes, nbytes);
  }

  return bytes;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "CMPS03_driver");
  CMPDriver CMPDrv;

  // Start Spinning
  ros::spin();

  return(0);
}
