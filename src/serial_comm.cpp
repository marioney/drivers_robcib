#include <drivers_robcib/serial_comm.h>

SerialComm::SerialComm(std::string port_name, int baud_rate=19200, int parity=0)
{
    port_name_ = port_name;
    baud_rate_ = baud_rate;
    parity_ = parity;

}
SerialComm::~SerialComm()
{

}
int SerialComm::openPort()
{
  int fd;								// File descriptor for the port
  speed_t baud_rate;
  struct termios options;


  fd = open(port_name_.c_str(), O_RDWR | O_NOCTTY );				// Open port for read and write not making it a controlling terminal
  if (fd == P_ERROR)
  {
    ROS_ERROR("CMPS Driver: Unable to open port %s, -- %s", port_name_.c_str(), strerror(errno));		// If open() returns an error
  }
  else
  {
    fcntl(fd, F_SETFL, 0);						// Get the current options for the port
    tcgetattr(fd, &options);
    switch (baud_rate_)
    {
    case 1200:
        baud_rate = B1200;
        break;
    case 1800:
        baud_rate = B1800;
        break;
    case 2400:
        baud_rate = B2400;
        break;
    case 4800:
        baud_rate = B4800;
        break;
    case 9600:
        baud_rate = B9600;
        break;
    case 19200:
        baud_rate = B19200;
        break;
    case 38400:
        baud_rate = B38400;
        break;
    case 57600:
        baud_rate = B57600;
        break;
    case 115200:
        baud_rate = B115200;
        break;
    case 230400:
        baud_rate = B230400;
        break;
    case 460800:
        baud_rate = B460800;
        break;
    case 500000:
        baud_rate = B500000;
        break;
    default:
        baud_rate = B19200;
        break;
    }

    cfsetispeed(&options, baud_rate);					// Set the baud rate

    options.c_cflag |= (CLOCAL | CREAD);				// Enable the receiver and set local mode

    switch (parity_) {
    case 0:
        // No parity (8N1):
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;
        break;
    case 1:
        // Even parity (7E1):
        options.c_cflag |= PARENB;
        options.c_cflag &= ~PARODD;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS7;
        break;
    case 2:
        // Odd parity (7O1):
        options.c_cflag |= PARENB;
        options.c_cflag |= PARODD;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS7;
        break;
    default:
        // No parity (8N1):
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;
        break;
    }
    tcsetattr(fd, TCSANOW, &options);				// Set the new options for the port
  }
  return (fd);
}

void SerialComm::closePort(int fd)
{
  if(close(fd) == P_ERROR)						// Close the port if it returns an error then display an error report
  {
    ROS_ERROR("CMPS Driver: Unable to close port %s,-- %s", port_name_.c_str(), strerror(errno));
    //perror("closePort: Unable to close port ");
  }
}

int SerialComm::writeData(int fd, int nbytes)
{
  int bytes;

  bytes = write(fd, sbufout, nbytes);					// Write nbytes of data from wbuf
  if(bytes == P_ERROR)							// If write returns an error (-1)
  {
    ROS_ERROR("writeData: Error while trying to write data - %s", strerror(errno));
  }
  else if(bytes != nbytes)
  {
    ROS_WARN("Only %u bytes written out of %u requested\n", bytes, nbytes);
  }
  return bytes;

}

int SerialComm::readData(int fd, int nbytes)
{
  int bytes;

  bytes = 0;

  //ROS_INFO("CMPS Driver: Trying");
  bytes = read(fd, sbufin, nbytes);
  if(bytes == P_ERROR) // If read returns and error (-1)
  {
    ROS_ERROR("readData: Error while trying to read data - %s", strerror(errno));
  }
  else if(bytes != nbytes)
  {
    ROS_WARN("Only %u bytes read out of %u requested", bytes, nbytes);
  }

  return bytes;
}

