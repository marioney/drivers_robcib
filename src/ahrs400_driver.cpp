#include <drivers_robcib/ahrs400_driver.h>

AHRS400Driver::AHRS400Driver()
{
    // Create the node handler
    ros::NodeHandle nh_private("~");

    int baud_rate;
    int parity;   // 0 - none; 1 - even;  2 - odd.
    std::string port_name;  // To store port device name
    std::string msg_topic;  // To store imu_topic

    // Read the parameters or default values
    nh_private.param("freq",freq, 2.0);
    nh_private.param("msg_topic", msg_topic, std::string("imu_data"));
    nh_private.param("port_name", port_name, std::string("/dev/ttyUSB0"));
    nh_private.param("baud_rate", baud_rate, 38400);
    nh_private.param("parity", parity, 0);

    nh_private.param("linear_acceleration_stdev", linear_acceleration_stdev_, 0.098);
    nh_private.param("orientation_stdev", orientation_stdev_, 0.035);
    nh_private.param("angular_velocity_stdev", angular_velocity_stdev_, 0.012);
    nh_private.param("frame_id", frame_id_, std::string("imu_link"));

    // Todo Define real values for each measure.

    double angular_velocity_covariance = angular_velocity_stdev_ * angular_velocity_stdev_;
    double orientation_covariance = orientation_stdev_ * orientation_stdev_;
    double linear_acceleration_covariance = linear_acceleration_stdev_ * linear_acceleration_stdev_;

    cmps_reading_.linear_acceleration_covariance[0] = linear_acceleration_covariance;
    cmps_reading_.linear_acceleration_covariance[4] = linear_acceleration_covariance;
    cmps_reading_.linear_acceleration_covariance[8] = linear_acceleration_covariance;

    cmps_reading_.angular_velocity_covariance[0] = angular_velocity_covariance;
    cmps_reading_.angular_velocity_covariance[4] = angular_velocity_covariance;
    cmps_reading_.angular_velocity_covariance[8] = angular_velocity_covariance;

    cmps_reading_.orientation_covariance[0] = orientation_covariance;
    cmps_reading_.orientation_covariance[4] = orientation_covariance;
    cmps_reading_.orientation_covariance[8] = orientation_covariance;

    cmps_reading_.header.frame_id = frame_id_;


    cmps_data_pub_ = nh_private.advertise<sensor_msgs::Imu>(msg_topic,1);

    ROS_INFO("AHRS400Driver: Parameters loaded");



    // Open Port
    ahrs_stream = ahrs_open(port_name.c_str());
    if (!ahrs_stream)
    {
        ROS_ERROR("CMPS Driver: ROS Node is Shutting Down");
        ros::shutdown();
    }
    ROS_INFO("AHRS400Driver: Port open");
    // Put AHRS into polled mode for configuration
    if (ahrs_set_polled(ahrs_stream))
    {
        ROS_ERROR("CMPS Driver: ROS Node is Shutting Down");
        ros::shutdown();
    }
    ROS_INFO("AHRS400Driver: Set polled");
    // Wait for pending data to arrive and clear buffers
    fflush(ahrs_stream);
    ROS_INFO("AHRS400Driver: fflush");
    sleep(1);
    ahrs_purge(ahrs_stream);
    ROS_INFO("AHRS400Driver: purge");

    // Ping the AHRS
    if (ahrs_ping(ahrs_stream))
    {
        ROS_ERROR("CMPS Driver: ROS Node is Shutting Down");
        ros::shutdown();
    }
    ROS_INFO("AHRS400Driver: Ping ok");


    // Set the mode
    if (ahrs_set_mode(ahrs_stream, AHRS400::AHRS_ANGLE_MODE)
            || ahrs_set_polled(ahrs_stream))
    {
        ROS_ERROR("CMPS Driver: ROS Node is Shutting Down");
        ros::shutdown();
    }
    ROS_INFO("AHRS400Driver: Set to continuous");




    timer_ = nh_private.createTimer(ros::Duration(1.0/freq), &AHRS400Driver::spin, this);

    ROS_INFO("AHRS400Driver: ROS Setup Completed");
}

AHRS400Driver::~AHRS400Driver()
{

}
void AHRS400Driver::spin(const ros::TimerEvent& e)
{
  ROS_DEBUG("AHRS400Driver::spin In the loop at time :%f", ros::Time::now().toSec());
  //elapsed_time = e.current_real.toSec()-e.last_real.toSec();

  drivers_robcib::AHRS400_ANGLE_RAW angle_raw;
  drivers_robcib::AHRS400_ANGLE angle;

  ahrs_request_data(ahrs_stream);

  if (ahrs_get_angle_raw(ahrs_stream, &angle_raw))
  {
      ROS_ERROR("AHRS400Driver::spin: Error reading angle data");
      return;
  }
  //ROS_INFO("AHRS400Driver::spin -  got data");

  ahrs_angle_conv(&angle_raw, &angle);
  ahrs_to_msg(angle);
  cmps_data_pub_.publish(cmps_reading_);  // Publish the data


}

void AHRS400Driver::ahrs_to_msg( const drivers_robcib::AHRS400_ANGLE angle)
{
    float roll;
    float pitch;
    float yaw;

    cmps_reading_.header.stamp = ros::Time::now();  // Add timestamp

    // Angular velocity (rad/s)
    cmps_reading_.angular_velocity.x = angle.xgyro;
    cmps_reading_.angular_velocity.y = angle.ygyro;
    cmps_reading_.angular_velocity.z = angle.zgyro;

    // Linear acceleration (rad/s^2)
    cmps_reading_.linear_acceleration.x = angle.xacc;
    cmps_reading_.linear_acceleration.y = angle.yacc;
    cmps_reading_.linear_acceleration.z = angle.zacc;

    // Angles (rad)
    roll  =  angle.roll;
    pitch = angle.pitch;
    yaw   = angle.yaw;

    ROS_INFO("AHRS400Driver::ahrs_to_msg: RPY: %.3f - %.3f - %.3f", roll, pitch, yaw );

    tf::quaternionTFToMsg(tf::createQuaternionFromRPY(roll, pitch, yaw), cmps_reading_.orientation); // Create quaternion from yaw

}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "CMPS03_driver");
  AHRS400Driver AHRS400Drv;

  // Start Spinning
  ros::spin();

  return(0);
}
